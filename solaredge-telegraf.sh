#!/bin/bash 
set -e

SITE_ID=$1
API_KEY=$2

# Get data from 15 minutes before now, so we get stable data
if [ `uname` == 'Darwin' ]; then
    DATE_MINUS_15M="-v -16M"
else
    DATE_MINUS_15M="--date -16minutes"
fi

startTime=$(date ${DATE_MINUS_15M} +"%Y-%m-%d %T")
endTime=$(date +"%Y-%m-%d %T")

power=$(curl -s -G https://monitoringapi.solaredge.com/site/${SITE_ID}/power \
    --data-urlencode "startTime=${startTime}" \
    --data-urlencode "endTime=${endTime}" \
    --data-urlencode "api_key=${API_KEY}" \
    | jq .power.values[0].value)
if [ $power == 'null' ]; then
  power=0
fi


# The `/energy` endpoint can only give us data for all 15 minute intervals for the 
# full day. We need to find the value for the interval we are interested in.
# To do this, we calculate the index of the current 15 minute interval.
# The calculation is `(HOURS * 60 + MINUTES - 15) / 15` which gives us a 
# number between 0 and 95.
# The weird `date` expression works by having `date` output an expression that can
# be evaluated by the shell to do the calculation.
# (credits: https://stackoverflow.com/questions/30370138/how-can-i-echo-minutes-since-midnight-in-any-timezone-using-the-date-command-on)
startDate=$(date ${DATE_MINUS_15M} +"%Y-%m-%d")
currentQuarter=$(( $(date ${DATE_MINUS_15M} "+(10#%H * 60 + 10#%M) / 15") ))

energy=$(curl -s -G https://monitoringapi.solaredge.com/site/${SITE_ID}/energy \
    --data-urlencode "startDate=${startDate}" \
    --data-urlencode "endDate=${startDate}" \
    --data-urlencode "api_key=${API_KEY}" \
    --data-urlencode "timeUnit=QUARTER_OF_AN_HOUR" \
    | jq .energy.values[${currentQuarter}].value)
if [ $energy == 'null' ]; then
  energy=0
fi

echo solaredge,site=${SITE_ID} power=${power},energy=${energy}
