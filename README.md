# solaredge-telegraf

Script to gather data from SolarEdge API and store in InfluxDB via Telegraf.

SolarEdge solar panel installations provide an API where power and energy
from the solar panels can be read. This little script can plug that data
into InfluxDB and you can then use Graphana (for example) to visualize the 
data.

## Requirements

The script requires `jq` to be installed.

## Installation

Edit `/etc/telegraf/telegraf.conf` and add something like the following:

    [[inputs.exec]]
        commands = [
          "/your/path/here/solaredge-telegraf.sh YOUR_SITE_ID YOUR_API_KEY"
        ]

        # SolarEdge data is only updated every 15 minutes
        interval = "15m"

        # Timeout for each command to complete.
        timeout = "5s"

        data_format = "influx"

The script expects your site id as the first parameter and your API key 
as the second parameter.
